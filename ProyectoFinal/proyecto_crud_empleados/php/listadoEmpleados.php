<?php
    include("conexion.php");
    $consulta = "SELECT * FROM emps";
    $lista = $conexion->query($consulta) or die("Error al consultar lista de empleados: <br>".$conexion->error);
?>

<table id="lista_productos">
    <tr>
        <th>ID</th>
        <th>Nombres</th>
        <th>Apellidos</th>
        <th>Direccion</th>
        <th>Puesto</th>
        <th>Sueldo</th>
        <th colspan="3">Opciones</th>
    </tr>
    <?php
        while ($arr_emps = $lista->fetch_row() ) {
          echo "<tr>";
          echo "<td>". $arr_emps[0] ."</td>";
          echo "<td>". $arr_emps[1] ."</td>";
          echo "<td>". $arr_emps[2] ."</td>";
          echo "<td>". $arr_emps[3] ."</td>";
          echo "<td>". $arr_emps[4] ."</td>";
          echo "<td>$". $arr_emps[5] ."</td>";
          echo '<td><button type="button" onclick="editEmp('.$arr_emps[0].');"><i class="fas fa-edit"></i></button></td>';
          echo '<td><button type="button" onclick="leerEmp('.$arr_emps[0].');"><i class="fas fa-tasks"></i></button></td>';
          echo '<td><button type="button" onclick="eliminarEmp('.$arr_emps[0].');"><i class="	fas fa-skull-crossbones"></i></button></td>';
          echo "</tr>";
        }
    ?>
</table>
