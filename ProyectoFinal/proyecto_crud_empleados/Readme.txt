********************************************
********************************************
****	Proyecto CRUD empleados en php	****
********************************************
********************************************
Por: Luis Felipe Cabello Galicia 

	BD 	empleados
	Table 	emps
	User 	root
	Pass 	No_hay 


La tabla emps se creó en phpmyadmin con las siguientes características:

	id_emp		INT PK AI
	nombres 	VARCHAR(50)
	apellidos	VARCHAR(50)
	direccion	TEXT
	puesto		VARCHAR(50)
	sueldo		INT

NOTA: Para ver el capturas del funcionamiento ver carpeta "Capturas funcionamiento"
	dentro de la carpeta del proyecto