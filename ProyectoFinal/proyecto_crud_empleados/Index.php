<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>CRUD - EMPLEADOS</title>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script> <!-- Agregamos jquery, el archivo de funciones js e iconos de font awesome -->
    <script type="text/javascript" src="js/funciones.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
  </head>
  <body>
    <h2>ADMINISTRADOR DE EMPLEADOS</h2>
    <br>
    <button type="button" onclick="nuevoEmp();"><i class="fas fa-file-medical"></i> Nuevo Empleado</button>
    <br><br>
    <div>
      <?php include("php/listadoEmpleados.php"); ?>
    </div>
    <br>
    <br>
    <footer>
        <p style="color:grey; text-align:center;">&copy; 2020 - Luis Felipe Cabello Galicia</p>
    </footer>
  </body>
</html>


<style type="text/css">
  h2,h4{color:rgba(150,150,150,.9);}
  th{padding:10px; background:rgba(0, 128, 128,.5); border-radius:5px; color:white;}
  td{padding:7px; background:rgba(225,225,225,.5); border-radius:5px; color:grey;}
  button{background:rgba(250,250,250,1);; border-color:rgba(225,0,0,0); color:rgba(0, 128, 128,.8); border-radius:5px;}
  button:hover{background:rgba(225,225,225,.15);}
  button:focus,button:active{color:rgba(0, 128, 128,.25);background: white;}
</style>
